#!/bin/bash

function __bluez_sink_mac_address () {
    # FIXME: this seems to only work if headphones are already
    # connected as sound sinks? need to find a better way of picking
    # up MAC-address. hcitool, perhaps? check `hcitool con|dev`.
    pacmd list-cards | grep bluez_card | sed 's/^[^\.]*\.//' | sed 's/>.*$//' | tr _ :
}

function __bluez_card_index () {
    pacmd list-cards | grep bluez_card -B1 | grep index | awk '{print $2}'
}

function bluez_headphones_stop () {
    DEVICE_MAC=$1
    if [[ -z "$DEVICE_MAC" ]]; then
        echo "Usage: bluez_headphones_stop MAC-ADDRESS"
        return
    fi

    # remove as sink
    HEADPHONES_CARD_INDEX=$(__bluez_card_index)
    pacmd set-card-profile $HEADPHONES_CARD_INDEX off

    # bluetooth disconnect
    echo "disconnect $DEVICE_MAC" | bluetoothctl
}

function bluez_headphones_start () {
    DEVICE_MAC=$1
    if [[ -z "$DEVICE_MAC" ]]; then
        echo "Usage: bluez_headphones_start MAC-ADDRESS"
        return
    fi

    # bluetooth (re-)connect
    echo "connect $DEVICE_MAC" | bluetoothctl

    # FIXME: avoid hardcoded timing, can wait for connection by
    # repeating list commands to bluetoothctl
    sleep 7

    # when connected, indicate to system that headphones should be a
    # HQ sink
    HEADPHONES_CARD_INDEX=$(__bluez_card_index)
    pacmd set-card-profile $HEADPHONES_CARD_INDEX a2dp_sink

    echo "NOTE: if shit still doesn't work, check play/pause and volume settings with the headphones' physical buttons."
}

function bluez_headphones_restart () {
    DEVICE_MAC=$(__bluez_sink_mac_address)
    echo MAC ADDRESS $DEVICE_MAC
    
    echo stopping...
    bluez_headphones_stop $DEVICE_MAC
    echo waiting...
    sleep 5 # FIXME: avoid hardcoded timing
    echo restarting...
    bluez_headphones_start $DEVICE_MAC
}

# DEBUG
#echo MAC ADDRESS $(__bluez_sink_mac_address)
#echo CARD INDEX $(__bluez_card_index)
